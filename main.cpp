/*
 * main.cpp
 * Copyright (C) 2018 Vyacheslav Korepanov <real93@live.ru>
 *
 * Distributed under terms of the MIT license.
 */

#include <cv.hpp>

using namespace cv;

std::vector<Rect> GetFaces(const Mat& image)
{
    CascadeClassifier cascade;
    cascade.load("haarcascade_frontalface_default.xml");

    Mat gray;
    cvtColor(image, gray, COLOR_BGR2GRAY);
    equalizeHist(gray, gray);

    std::vector<Rect> faces;
    cascade.detectMultiScale(gray, faces);
    return faces;
}

int main()
{
    //constexpr auto imgName = "/media/data/userFiles/pictures/Ширяево 2015/Ширяево 2015/Фото/20150801101931.jpg";
    //constexpr auto imgName = "/media/data/userFiles/pictures/Ширяево 2015/Ширяево 2015/Фото/20150801101620.jpg";
    constexpr auto imgName = "/media/data/userFiles/pictures/Ширяево 2015/Ширяево 2015/Фото/20150801095138.jpg";
    constexpr auto windowName = "Preview";

    auto image = imread(imgName, IMREAD_COLOR);

    const auto color = Scalar(255, 0, 0);
    const auto faces = GetFaces(image);
    for (const auto& face : faces) {
        const auto radius = (face.width + face.height) / 3.0;
        circle(image, cvPoint(cvRound(face.x + face.width / 2), cvRound(face.y + face.height / 2)), radius, color, 10);
    }

    namedWindow(windowName, WINDOW_NORMAL);
    resizeWindow(windowName, 1920, 1080);
    imshow(windowName, image);

    char c = 'a';
    while (c != 'q')
        c = cvWaitKey(0);

    return 0;
}
